namespace MapperExtensions.Models
{
    public class IdentityCard
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}