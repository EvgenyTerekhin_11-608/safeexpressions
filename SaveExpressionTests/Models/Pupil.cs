namespace MapperExtensions.Models
{
    public class Pupil
    {
        public int Id { get; set; }
        public IdentityCard Identity;
        public AddressCard Address;
    }
}