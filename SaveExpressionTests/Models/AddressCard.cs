namespace MapperExtensions.Models
{
    public class AddressCard
    {
        public string Id { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Street { get; set; }
        public int House { get; set; }
    }
}